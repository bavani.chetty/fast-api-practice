from fastapi import FastAPI
from fastapi import Path
from typing import Optional
from pydantic import BaseModel

app = FastAPI()


class Thing(BaseModel):
    name: str


list_of_things = [
    Thing(name='Saturn'),
    Thing(name='Kajal')
]


@app.get("/")
def home():
    return {"hello": 'world'}


@app.get("/things/{thing_name}")
def get_thing(thing_name: str = Path(None, description="Name of the thing")):
    return next(filter(lambda x: x.name == thing_name, list_of_things), {})


@app.get("/things")
def get_thing(thing_id: Optional[int] = None):
    return {"whatever": thing_id}


@app.post("/create_thing")
def create_thing(thing: Thing):
    list_of_things.append(thing)
    return "yay"
